package ru.t1.amsmirnov.taskmanager.service;

import ru.t1.amsmirnov.taskmanager.api.repository.ITaskRepository;
import ru.t1.amsmirnov.taskmanager.api.service.ITaskService;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.TaskNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.*;
import ru.t1.amsmirnov.taskmanager.model.Task;

import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
    }

    @Override
    public Task create(final String userId, final String name, final String description) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(userId, task);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return repository.findAllByProjectId(userId, projectId);
    }

    @Override
    public Task updateById(final String userId, final String id, final String name, final String description) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final String userId, final Integer index, final String name, final String description) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null) throw new IndexIncorrectException();
        if (index < 0 || index > repository.getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeStatusById(final String userId, final String id, final Status status) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final String userId, final Integer index, final Status status) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null) throw new IndexIncorrectException();
        if (index < 0 || index > repository.getSize(userId)) throw new IndexIncorrectException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

}
