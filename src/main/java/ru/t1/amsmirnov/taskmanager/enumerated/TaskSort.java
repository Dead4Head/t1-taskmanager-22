package ru.t1.amsmirnov.taskmanager.enumerated;

import ru.t1.amsmirnov.taskmanager.comparator.CreatedComparator;
import ru.t1.amsmirnov.taskmanager.comparator.NameComparator;
import ru.t1.amsmirnov.taskmanager.comparator.StatusComparator;
import ru.t1.amsmirnov.taskmanager.model.Task;

import java.util.Comparator;

public enum TaskSort {

    BY_NAME("TaskSort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("TaskSort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("TaskSort by created", CreatedComparator.INSTANCE::compare),
    BY_DEFAULT("Default sort", null);

    private final String name;

    private final Comparator<Task> comparator;

    public static TaskSort toSort(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final TaskSort sort : values()) {
            if (sort.name().equals(value) || sort.name.equals(value)) return sort;
        }
        return null;
    }

    TaskSort(final String name, final Comparator<Task> comparator) {
        this.name = name;
        this.comparator = comparator;
    }

    public String getName() {
        return name;
    }

    public Comparator<Task> getComparator() {
        return comparator;
    }

}
