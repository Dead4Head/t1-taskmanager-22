package ru.t1.amsmirnov.taskmanager.api.repository;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model) throws AbstractException;

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    M findOneById(String id) throws AbstractException;

    M findOneByIndex(Integer index) throws AbstractException;

    void removeAll();

    void removeAll(Collection<M> collection);

    M removeOne(M model) throws AbstractException;

    M removeOneById(String id) throws AbstractException;

    M removeOneByIndex(Integer index) throws AbstractException;

    Integer getSize();

    Boolean existById(String id) throws AbstractException;

}
