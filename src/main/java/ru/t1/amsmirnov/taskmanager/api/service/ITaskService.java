package ru.t1.amsmirnov.taskmanager.api.service;

import ru.t1.amsmirnov.taskmanager.api.repository.ITaskRepository;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.Task;

public interface ITaskService extends IUserOwnedService<Task>, ITaskRepository {

    Task create(String userId, String name, String description) throws AbstractException;

    Task updateById(String userId, String id, String name, String description) throws AbstractException;

    Task updateByIndex(String userId, Integer index, String name, String description) throws AbstractException;

    Task changeStatusById(String userId, String id, Status status) throws AbstractException;

    Task changeStatusByIndex(String userId, Integer index, Status status) throws AbstractException;

}
