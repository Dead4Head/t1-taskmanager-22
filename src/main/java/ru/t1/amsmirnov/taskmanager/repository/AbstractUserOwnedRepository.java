package ru.t1.amsmirnov.taskmanager.repository;

import ru.t1.amsmirnov.taskmanager.api.repository.IUserOwnedRepository;
import ru.t1.amsmirnov.taskmanager.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public M add(final String userId, final M model) {
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public List<M> findAll(final String userId) {
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public M findOneById(final String userId, final String id) {
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return records
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Override
    public M removeOne(final String userId, final M model) {
        return removeOneById(userId, model.getId());
    }

    @Override
    public M removeOneById(final String userId, final String id) {
        final M model = findOneById(userId, id);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Override
    public M removeOneByIndex(final String userId, final Integer index) {
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Override
    public boolean existById(final String userId, final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public void removeAll(final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public int getSize(final String userId) {
        return (int) records
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

}
