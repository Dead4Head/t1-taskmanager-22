package ru.t1.amsmirnov.taskmanager.repository;

import ru.t1.amsmirnov.taskmanager.api.repository.IRepository;
import ru.t1.amsmirnov.taskmanager.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> records = new ArrayList<>();

    @Override
    public M add(final M model) {
        records.add(model);
        return model;
    }

    @Override
    public List<M> findAll() {
        return records;
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        return records
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public void removeAll() {
        records.clear();
    }

    @Override
    public void removeAll(Collection<M> collection) {
        if (collection == null) return;
        records.removeAll(collection);
    }

    @Override
    public M findOneById(final String id) {
        return records
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        return records.get(index);
    }

    @Override
    public M removeOne(final M model) {
        if (model == null) return null;
        records.remove(model);
        return model;
    }

    @Override
    public M removeOneById(final String id) {
        final M model = findOneById(id);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Override
    public M removeOneByIndex(final Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) return null;
        removeOne(model);
        return model;
    }

    @Override
    public Integer getSize() {
        return records.size();
    }

    @Override
    public Boolean existById(final String id) {
        final M model = findOneById(id);
        return !(model == null);
    }

}
