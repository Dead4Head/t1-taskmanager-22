package ru.t1.amsmirnov.taskmanager.command.user;

import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    public static final String NAME = "user-unlock";
    public static final String DESCRIPTION = "Unlock user by login.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER USER LOGIN:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockUserByLogin(login);
    }

}
