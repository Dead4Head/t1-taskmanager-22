package ru.t1.amsmirnov.taskmanager.command.task;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public final class TaskClearCommand extends AbstractTaskCommand {

    public static final String NAME = "task-clear";
    public static final String DESCRIPTION = "Clear task list.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CLEAR TASK LIST]");
        final String userId = getUserId();
        getTaskService().removeAll(userId);
    }

}
