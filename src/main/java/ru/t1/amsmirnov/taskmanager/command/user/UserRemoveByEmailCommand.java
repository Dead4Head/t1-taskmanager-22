package ru.t1.amsmirnov.taskmanager.command.user;

import ru.t1.amsmirnov.taskmanager.api.service.IUserService;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

public final class UserRemoveByEmailCommand extends AbstractUserCommand {

    public static final String NAME = "user-remove-by-email";
    public static final String DESCRIPTION = "Remove user by email.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE USER BY LOGIN]");
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        final IUserService userService = serviceLocator.getUserService();
        userService.removeByEmail(email);
    }

}
