package ru.t1.amsmirnov.taskmanager.command.project;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public final class ProjectClearCommand extends AbstractProjectCommand {

    public static final String NAME = "project-clear";
    public static final String DESCRIPTION = "Clear project list.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CLEAR PROJECTS LIST]");
        final String userId = getUserId();
        getProjectService().removeAll(userId);
    }

}
